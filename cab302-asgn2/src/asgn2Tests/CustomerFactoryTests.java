package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 * 
 * @author Person A
 *
 */
public class CustomerFactoryTests {
	//created passing vars
	private static final String NumPass  = "0123456789";
	private static final int LocXPass = 5;
	private static final int LocYPass =	-7;
	private static final String NamePass  = "Conrad Von Hosindor";
	
	// this tests the exception when the Costumer code is wrong
	@Test(expected = CustomerException.class)
	public void customerFactoryWhenCodeWrong() throws CustomerException{				
		Customer Test = CustomerFactory.getCustomer("WOW", NamePass, NumPass, LocXPass, LocYPass);
	}
	
	// this tests the exception when  the Costumer code for pickup
	@Test(expected = CustomerException.class)
	public void customerFactoryWhenCodePickUp() throws CustomerException{				
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocYPass);
		assertEquals(Test.getCustomerType(),"PUC");
	}
	
	// this tests the exception when  the Costumer code for Drone
	@Test(expected = CustomerException.class)
	public void customerFactoryWhenCodeDrone() throws CustomerException{				
		Customer Test = CustomerFactory.getCustomer("DNC", NamePass, NumPass, LocXPass, LocYPass);
		assertEquals(Test.getCustomerType(),"DNC");
	}
	
	// this tests the exception when  the Costumer code for Driver delivery
	@Test(expected = CustomerException.class)
	public void customerFactoryWhenCodeDriver() throws CustomerException{				
		Customer Test = CustomerFactory.getCustomer("DVC", NamePass, NumPass, LocXPass, LocYPass);
		assertEquals(Test.getCustomerType(),"DVC");
	}
}
