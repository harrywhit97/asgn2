package asgn2Tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalTime;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/** 
 * A class that tests the asgn2Pizzas.PizzaFactory class.
 * 
 * @author Person B 
 * 
 */
public class PizzaFactoryTests {
	// TO DO	

	private static final int QUANTITY = 2;
	private static final LocalTime VALID_ORDER_TIME = LocalTime.parse("22:01:00");
	private static final LocalTime VALID_DELIVER_TIME = LocalTime.parse("22:25:00");
	
	// tests exception thrown when the pizza code is wrong
	@Test(expected = PizzaException.class)
	public void exceptionPizzaFactoryCodeWrong() throws PizzaException{				
		Pizza pizza = PizzaFactory.getPizza("INT", QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
	}
		
}
