package asgn2Tests;

import static org.junit.Assert.*;

import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;
import asgn2Restaurant.LogHandler;

/** A class that tests the methods relating to the creation of Pizza objects in the asgn2Restaurant.LogHander class.
* 
* @author Person B
* 
*/
public class LogHandlerPizzaTests {
	// TO DO	
	final String fileName = "logs/20170101.txt";
	final String[] orders = {"19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2",
								"20:00:00,20:25:00,April O'Neal,0987654321,DNC,3,4,PZM,1",
								"21:00:00,21:35:00,Oroku Saki,0111222333,PUC,0,0,PZL,3"};
	final int numOrders = 3;
	private static String BadLine = "asdf,asdf,asdfasf";
	
	
	//Create Pizza throws exception when time in not correct and fails parse
	@Test(expected = LogHandlerException.class)
	public void createPizzaExceptionBadTime() throws PizzaException, LogHandlerException{		
		String  line = "19.00:00,one:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
		Pizza p = LogHandler.createPizza(line);
	}
	
	//Create Pizza throws exception when quantity in not correct and fails parse
	@Test(expected = LogHandlerException.class)
	public void createPizzaExceptionBadQuantity() throws PizzaException, LogHandlerException{		
		String  line = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,two";
		Pizza p = LogHandler.createPizza(line);
	}
	
	
	//populatePizzaDataset
	@Test
	public void populatePizzaDatasetMakesPizzas(){
		ArrayList<Pizza> pList = null;
		try {
			pList = LogHandler.populatePizzaDataset(fileName);
		} catch (PizzaException | LogHandlerException e) {
			e.printStackTrace();
		}
		
		boolean pizzaInfoCorrect = true;
		
		Pizza pizzas[] = new Pizza[numOrders];
		for(int i = 0; i < numOrders; i++){
			try {
				pizzas[i] = LogHandler.createPizza(orders[i]);
			} catch (PizzaException | LogHandlerException e){
				e.printStackTrace();
			}
		}
		
		int count = 0;
		for(Pizza p : pList){
			if(!p.equals(pizzas[count++])){
				pizzaInfoCorrect = false;
				break;
			}
		}
		assertTrue(pizzaInfoCorrect);		
	}
	
	//test the exception is thrown when text document is wrong
	@Test(expected = LogHandlerException.class)
	public void TestBadLogFile() throws PizzaException, LogHandlerException{
		String BadLogFile = "./logs/badLogFile.txt";
		LogHandler.populatePizzaDataset(BadLogFile);
	}
	
	//test the Pizza is exception
	@Test(expected = LogHandlerException.class)
	public void TestPizzaIsReturnedexception() throws LogHandlerException, PizzaException{
		
		Pizza c = LogHandler.createPizza(BadLine);			
	}
	
	//test the Pizza is returned
	@Test
	public void TestPizzaIsReturned() throws LogHandlerException, PizzaException{
		
		Pizza c = LogHandler.createPizza(orders[0]);
		assertEquals(c, PizzaFactory.getPizza("PZV", 2, LocalTime.parse("19:00:00"), LocalTime.parse("19:20:00")));
	}
	
}
