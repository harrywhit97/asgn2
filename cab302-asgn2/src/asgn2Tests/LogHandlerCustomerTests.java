package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;
import asgn2Restaurant.LogHandler;
import asgn2Customers.CustomerFactory;


/**
 * A class that tests the methods relating to the creation of Customer objects in the asgn2Restaurant.LogHander class.
 *
 * @author Person A
 */
public class LogHandlerCustomerTests {
	
	
	private static String BadLine = "asdf,asdf,asdfasf";
	private static String GoodLine = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
	
	//test the exception is thrown when text document is wrong
	@Test(expected = LogHandlerException.class)
	public void TestBadLogFile() throws LogHandlerException, CustomerException{
		String BadLogFile = "./logs/badLogFile.txt";
		LogHandler.populateCustomerDataset(BadLogFile);
	}
	
	//test the Customer is exception
	@Test(expected = LogHandlerException.class)
	public void TestComstomerIsReturnedexception() throws LogHandlerException, CustomerException{
		
		Customer c = LogHandler.createCustomer(BadLine);			
	}
	
	//test the Customer is returned
	@Test
	public void TestComstomerIsReturned() throws LogHandlerException, CustomerException{
		
		Customer c = LogHandler.createCustomer(GoodLine);
		assertEquals(c, CustomerFactory.getCustomer("DVC", "Casey Jones", "0123456789", 5, 5));
	}
}
