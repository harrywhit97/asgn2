package asgn2Tests;

import asgn2Pizzas.*;
import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;
import asgn2Exceptions.PizzaException;

/**
 * A class that that tests the asgn2Pizzas.MargheritaPizza, asgn2Pizzas.VegetarianPizza, asgn2Pizzas.MeatLoversPizza classes. 
 * Note that an instance of asgn2Pizzas.MeatLoversPizza should be used to test the functionality of the 
 * asgn2Pizzas.Pizza abstract class. 
 * 
 * @author Person B
 *
 */
public class PizzaTests {
	// TO DO
	
	
	private static final double COST_FOR_M = 1.5;
	private static final double COST_FOR_L = 5.0;
	private static final double COST_FOR_V = 5.5;
	
	private static final double PRICE_FOR_M = 8.0;
	private static final double PRICE_FOR_L = 12.0;
	private static final double PRICE_FOR_V = 10.0;
	private static final int QUANTITY = 1;
	
	private static final LocalTime VALID_ORDER_TIME = LocalTime.parse("22:01:00");
	private static final LocalTime VALID_DELIVER_TIME = LocalTime.parse("22:25:00");
	
	

	
	private static final String CODE = "PZL";
	//test that exception is thrown when quantity is or exceeds 10
	@Test(expected = PizzaException.class)
	public void constructorExceptionWhenQuantityTooBig() throws PizzaException{	
		Pizza p = PizzaFactory.getPizza(CODE, 20, LocalTime.parse("22:01:00"), LocalTime.parse("22:11:00"));
	}
	
	//test the exception is thrown when quantity is less than 0
	@Test(expected = PizzaException.class)
	public void constructorExceptionWhenQuantityTooSmall() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, -1, VALID_ORDER_TIME, VALID_DELIVER_TIME);
	}
	
	//test the exception is thrown when order time is before 19:00 
	@Test(expected = PizzaException.class)
	public void constructorExceptionVALID_ORDER_TIMEBefore1900() throws PizzaException{
		LocalTime orderTime = LocalTime.parse("18:59:59");
		LocalTime deliverTime = LocalTime.parse("19:10:00");		
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, orderTime, deliverTime);
	}
	
	//test the exception is thrown when order time is after 23:00
	@Test(expected = PizzaException.class)
	public void ExceptionConstructorOrderTimeAfter2300() throws PizzaException{
		LocalTime VALID_ORDER_TIME = LocalTime.parse("23:00:01");
		LocalTime VALID_DELIVER_TIME = LocalTime.parse("23:10:00");		
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
	}
	
	//test the exception is thrown when deliveryTime is before order time
	@Test(expected = PizzaException.class)
	public void constructorExceptionDelivTimeBeforeOrderTime() throws PizzaException{
		LocalTime deliverTime = LocalTime.parse("21:16:00");		
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, deliverTime);
	}
			
	//test the exception is thrown when order time is after 23:00 
	@Test(expected = PizzaException.class)
	public void constructorExceptionOrderTimeAfter2300() throws PizzaException{
		LocalTime orderTime = LocalTime.parse("23:01:00");
		LocalTime deliverTime = LocalTime.parse("23:27:00");		
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, orderTime, deliverTime);
	}
	
	//test the exception is thrown when deliveryTime is one hour after VALID_ORDER_TIME
	@Test(expected = PizzaException.class)
	public void constructorExceptionDelivTime1HourAfterOrder() throws PizzaException{
		LocalTime deliverTime = LocalTime.parse("23:01:01");		
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, deliverTime);
	}
	
	//test the exception is thrown when pizza type is invalid
	@Test(expected = PizzaException.class)
	public void constructorExceptionInvalidType() throws PizzaException{	
		Pizza p = PizzaFactory.getPizza("PPP", QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
	}
	
	//get cost per pizza returns correct cost for PZL also tests calc cost per pizza
	@Test
	public void getCostPerPizzaCorrectPZL() throws PizzaException{		
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getCostPerPizza();
		double expectedAnswer  = COST_FOR_L;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get cost per pizza returns correct cost for PZL also tests calc cost per pizza multiple times
	@Test
	public void getCostPerPizzaCorrectPZLMultipleTmes() throws PizzaException{		
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getCostPerPizza();
		runAnswer  = p.getCostPerPizza();
		runAnswer  = p.getCostPerPizza();
		double expectedAnswer  = COST_FOR_L;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get cost per pizza returns correct cost for PZL also tests calc cost per pizza
	@Test
	public void getCostPerPizzaCorrectPZV() throws PizzaException{		
		Pizza p = PizzaFactory.getPizza("PZV", QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getCostPerPizza();
		double expectedAnswer  = COST_FOR_V;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get cost per pizza returns correct cost for PZL also tests calc cost per pizza
	@Test
	public void getCostPerPizzaCorrectPZM() throws PizzaException{		
		Pizza p = PizzaFactory.getPizza("PZM", QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getCostPerPizza();
		double expectedAnswer  = COST_FOR_M;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get price per pizza returns correct cost for PZL
	@Test
	public void getPricePerPizzaCorrectPZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getPricePerPizza();
		double expectedAnswer  = PRICE_FOR_L;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
		
	//get order cost returns correct cost for 1 of PZL
	@Test
	public void getOrderCostCorrect1PZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getOrderCost();
		double expectedAnswer  = COST_FOR_L;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get order cost returns correct cost for 2 of PZL
	@Test
	public void getOrderCostCorrect2PZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, 2, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getOrderCost();
		double expectedAnswer  = COST_FOR_L * 2.0;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get order cost returns correct cost for 3 of PZL
	@Test
	public void getOrderCostCorrect3PZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, 3, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getOrderCost();
		double expectedAnswer  = COST_FOR_L * 3.0;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
		
	//get order price returns correct cost for 1 of PZL
	@Test
	public void getOrderPriceCorrect1PZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getOrderPrice();
		double expectedAnswer  = PRICE_FOR_L;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get order price returns correct cost for 2 of PZL
	@Test
	public void getOrderPriceCorrect2PZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, 2, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getOrderPrice();
		double expectedAnswer  = PRICE_FOR_L * 2.0;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get order price returns correct cost for 3 of PZL
	@Test
	public void getOrderPriceCorrect3PZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, 3, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getOrderPrice();
		double expectedAnswer  = PRICE_FOR_L * 3.0;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//get order profit returns correct for PZL
	@Test
	public void getOrderProfitCorrectPZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		double runAnswer  = p.getOrderProfit();
		double expectedAnswer  =  PRICE_FOR_L - COST_FOR_L;
		assertEquals(runAnswer, expectedAnswer, 0);
	}
	
	//Contains toppings PZL
	@Test
	public void containsToppingsPZL() throws PizzaException{
		Pizza p = PizzaFactory.getPizza(CODE, QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		
		PizzaTopping[] PZLToppings = {PizzaTopping.CHEESE,PizzaTopping.TOMATO,PizzaTopping.BACON,
				PizzaTopping.PEPPERONI, PizzaTopping.SALAMI};
		Boolean hasAllToppings = true;
		for(PizzaTopping top : PZLToppings){
			if(!p.containsTopping(top)){
				hasAllToppings = false;
				break;
			}
		}
		assertTrue(hasAllToppings);
	}
	
	//Contains toppings PZM
	@Test
	public void containsToppingsPZM() throws PizzaException{
		Pizza p = PizzaFactory.getPizza("PZM", QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		
		PizzaTopping[] PZMToppings = {PizzaTopping.CHEESE,PizzaTopping.TOMATO};	
		Boolean hasAllToppings = true;
		for(PizzaTopping top : PZMToppings){
			if(!p.containsTopping(top)){
				hasAllToppings = false;
				break;
			}
		}
		assertTrue(hasAllToppings);
	}
	
	//Contains toppings PZV
	@Test
	public void containsToppingsPZV() throws PizzaException{
		Pizza p = PizzaFactory.getPizza("PZV", QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		
		PizzaTopping[] PZVToppings = {PizzaTopping.CHEESE,PizzaTopping.TOMATO, PizzaTopping.EGGPLANT,
				PizzaTopping.MUSHROOM, PizzaTopping.CAPSICUM};
		Boolean hasAllToppings = true;
		for(PizzaTopping top : PZVToppings){
			if(!p.containsTopping(top)){
				hasAllToppings = false;
				break;
			}
		}
		assertTrue(hasAllToppings);
	}	
	
	//get quantity returns correct value
	@Test	
	public void getQuantityCorrect() throws PizzaException{
		Boolean correct = true;
		for(int i = 1; i < 3; i++){
			Pizza p = PizzaFactory.getPizza("PZL", i, VALID_ORDER_TIME, VALID_DELIVER_TIME);
			if(p.getQuantity() != i){
				correct = false;
				break;
			}
		}
		Pizza p = PizzaFactory.getPizza("PZV", QUANTITY, VALID_ORDER_TIME, VALID_DELIVER_TIME);
		assertTrue(correct);
	}
}