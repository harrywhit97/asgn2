package asgn2Tests;

import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer, asgn2Customers.DriverDeliveryCustomer,
 * asgn2Customers.DroneDeliveryCustomer classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer 
 * should be used to test the functionality of the  asgn2Customers.Customer abstract class. 
 * 
 * @author Person A
 * 
 *
 */



public class CustomerTests {
	
	//created passing vars
	private static final String NumPass  = "0123456789";
	private static final int LocXPass = 5;
	private static final int LocYPass =	-7;
	private static final String NamePass  = "Conrad Von Hosindorf";
	
	// this tests the exception when the name is too long
	@Test(expected = CustomerException.class)
	public void customerWhenNameTooLong() throws CustomerException{
		String name = "abcdefghijklmnopqrstu";		
		Customer Test = CustomerFactory.getCustomer("PUC", name, NumPass, LocXPass, LocYPass);
	}
	
	// this tests the exception when the name is too short
	@Test(expected = CustomerException.class)
	public void customerWhenNameTooshort() throws CustomerException{
		String name = "";		
		Customer Test = CustomerFactory.getCustomer("PUC", name, NumPass, LocXPass, LocYPass);
	}
	
	// this tests the exception when the name is just white spaces
	@Test(expected = CustomerException.class)
	public void customerWhenNameWhiteSpace() throws CustomerException{
		String name = "     ";		
		Customer Test = CustomerFactory.getCustomer("PUC", name, NumPass, LocXPass, LocYPass);
	}
	
	// this tests the exception when the number is too short
	@Test(expected = CustomerException.class)
	public void customerWhenNumberTooshort() throws CustomerException{
		String num = "01234";		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, num, LocXPass, LocYPass);
	}
	
	// this tests the exception when the number starts w/o a 0
	@Test(expected = CustomerException.class)
	public void customerWhenNumberNo0() throws CustomerException{
		String num = "1234567890";		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, num, LocXPass, LocYPass);
	}
	
	// this tests the exception when the Locationx too big
	@Test(expected = CustomerException.class)
	public void customerWhenLocXTooBig() throws CustomerException{
		int LocX = 11	;
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocX, LocYPass);
	}
	
	// this tests the exception when the Locationx too Small
	@Test(expected = CustomerException.class)
	public void customerWhenLocXTooSmall() throws CustomerException{
		int LocX = -11;
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocX, LocYPass);
	}
	
	// this tests the exception when the LocationY too Small
	@Test(expected = CustomerException.class)
	public void customerWhenLocYTooSmall() throws CustomerException{
		int LocY = -11;
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocY);
	}
	
	// this tests the exception when the LocationY too Large
	@Test(expected = CustomerException.class)
	public void customerWhenLocYTooLarge() throws CustomerException{
		int LocY = 11;
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocY);
	}
	
	// this tests the Getname Function
	@Test
	public void customerGetName() throws CustomerException{
		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocYPass);
		assertEquals(Test.getName(),NamePass);
	}
	
	// this tests the Getmobile  Function
	@Test
	public void customerGetmobile() throws CustomerException{
		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocYPass);
		assertEquals(Test.getMobileNumber() ,NumPass);
	}
	
	// this tests the GetcustomerType Function
	@Test
	public void customerCustomerType() throws CustomerException{
		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocYPass);	
		assertEquals(Test.getCustomerType() ,"PUC");
	}
	
	// this tests the getLocationX()Function
	@Test
	public void customerGetLocationX() throws CustomerException{
		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocYPass);	
		assertEquals(Test.getLocationX() , LocXPass);
	}
	
	// this tests the ggetLocationY()Function
	@Test
	public void customerGetLocationY() throws CustomerException{
		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocYPass);	
		assertEquals(Test.getLocationY() , LocYPass);
	}
	
	// this tests the getDeliveryDistance() Function for pickup
	@Test
	public void customergetDeliveryDistancePickUp() throws CustomerException{
		
		Customer Test = CustomerFactory.getCustomer("PUC", NamePass, NumPass, LocXPass, LocYPass);	
		double TestAnswer = 0;
		assertEquals(Test.getDeliveryDistance() , TestAnswer,0);		 
	}
	
	// this tests the getDeliveryDistance() Function for Driver
	@Test
	public void customergetDeliveryDistanceDriver() throws CustomerException{		
		Customer Test = CustomerFactory.getCustomer("DVC", NamePass, NumPass, LocXPass, LocYPass);	
		double TestAnswer = 12;
		assertEquals(Test.getDeliveryDistance() , TestAnswer,0);		 
	}
	
	// this tests the getDeliveryDistance() Function for Drone
	@Test
	public void customergetDeliveryDistanceDrone() throws CustomerException{		
		Customer Test = CustomerFactory.getCustomer("DNC", NamePass, NumPass, -2, 1);	
		double TestAnswer = Math.sqrt((-2^2)+(1^2));
		double TestReal = Test.getDeliveryDistance();
		assertEquals(TestReal , TestAnswer, 0);		 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
