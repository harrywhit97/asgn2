package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.LogHandler;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that that tests the methods relating to the handling of Customer objects in the asgn2Restaurant.PizzaRestaurant
 * class as well as processLog and resetDetails.
 * 
 * @author Person A
 */
public class RestaurantCustomerTests {
	//getPizzaByIndex return corrrect customer
	@Test
	public void getCustomerByIndex() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		String  line = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
		r.processLog("./logs/20170101.txt");
		Customer C = r.getCustomerByIndex(0);
		Customer C1 = LogHandler.createCustomer(line);
		assertTrue(C.equals(C1));		
	}
	
	//getPizzaByIndex throws exception for wrong index
	@Test(expected = CustomerException.class)
	public void gteCustomerByIndexException() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		String  line = "19.00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
		r.processLog("./logs/20170101.txt");
		Customer c = r.getCustomerByIndex(100);	
	}
	
	
	//getNumPizzaOrders
	//getNumPizzaOrders return corrrect number
	@Test
	public void getNumCustomerOrdersCorrect() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		r.processLog("./logs/20170101.txt");
		assertEquals(r.getNumCustomerOrders(), 3);		
	}

	//getTotalDeliveryDistance
	@Test
	public void getgetTotalDeliveryDistance() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		String fileName = "./logs/20170101.txt";
		r.processLog(fileName);
		double distance = Math.sqrt(((Math.abs(3)^2) + ((Math.abs(4)^2))));
		distance += 10;				
		assertEquals(r.getTotalDeliveryDistance(),distance , 0);		
	}
}