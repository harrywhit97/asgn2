package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.LogHandler;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that tests the methods relating to the handling of Pizza objects in the asgn2Restaurant.PizzaRestaurant class as well as
 * processLog and resetDetails.
 * 
 * @author Person B
 *
 */
public class RestaurantPizzaTests {
	// TO DO
	
	//getPizzaByIndex return corrrect pizza
	@Test
	public void getPizzaByIndex() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		String  line = "19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
		r.processLog("./logs/20170101.txt");
		Pizza p = r.getPizzaByIndex(0);
		Pizza p1 = LogHandler.createPizza(line);
		assertTrue(p.equals(p1));		
	}
	
	//getPizzaByIndex throws exception for wrong index
	@Test(expected = PizzaException.class)
	public void getPizzaByIndexException() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		String  line = "19.00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2";
		r.processLog("./logs/20170101.txt");
		Pizza p = r.getPizzaByIndex(100);	
	}
	
	
	//getNumPizzaOrders
	//getNumPizzaOrders return corrrect number
	@Test
	public void getNumPizzaOrdersCorrect() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		r.processLog("./logs/20170101.txt");
		assertEquals(r.getNumPizzaOrders(), 3);		
	}

	//getTotalProfit	
	@Test
	public void getTotalProfitCorrect() throws PizzaException, CustomerException, LogHandlerException{
		PizzaRestaurant r = new PizzaRestaurant();
		String fileName = "./logs/20170101.txt";
		r.processLog(fileName);
		ArrayList<Pizza> p = LogHandler.populatePizzaDataset(fileName);
		double sum = 0.0;
		for(Pizza pp : p){
			sum += pp.getOrderProfit();
		}			
		assertEquals(r.getTotalProfit(), sum, 0);		
	}
}
