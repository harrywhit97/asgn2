package asgn2Restaurant;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/**
 *
 * A class that contains methods that use the information in the log file to return Pizza 
 * and Customer object - either as an individual Pizza/Customer object or as an
 * ArrayList of Pizza/Customer objects.
 * 
 * @author Person A and Person B
 *
 */
public class LogHandler {
	


	/**
	 * Returns an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file.
	 * @param filename The file name of the log file
	 * @return an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file. 
	 * @throws CustomerException If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above
	 * 
	 */
	public static ArrayList<Customer> populateCustomerDataset(String filename) throws CustomerException, LogHandlerException{
		// TO DO
		String thisLine = null;
	    ArrayList<Customer> customers = new ArrayList<>();
		try {
		 
		  // open input stream test.txt for reading purpose.
		  BufferedReader br = new BufferedReader(new FileReader(filename));
		  
		  while ((thisLine = br.readLine()) != null) {		   
		     customers.add(createCustomer(thisLine));
		  }  
		  br.close();
		} catch(IOException e) {
		  
		   throw new LogHandlerException(e.getMessage());
		}
		return customers;	   
	}		

	/**
	 * Returns an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @param filename The file name of the log file
	 * @return an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above
	 * 
	 */
	public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException{
		// TO DO
		String rawText;
		ArrayList<Pizza> Pizzas = new ArrayList<>();
		try{			 
			BufferedReader br = new BufferedReader(new FileReader(filename));
		  
			while ((rawText = br.readLine()) != null) {		   
				Pizzas.add(createPizza(rawText));
				}  
			br.close();
		} catch(IOException e) {
			  
			   throw new LogHandlerException(e.getMessage());
			}
		return Pizzas;			
	}		

	
	/**
	 * Creates a Customer object by parsing the  information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Customer object containing the information from the line in the log file
	 * @throws CustomerException - If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException - If there was a problem parsing the line from the log file.
	 */
	public static Customer createCustomer(String line) throws CustomerException, LogHandlerException{
		// TO DO
		String customerInfo[] = line.split(",");
		
		if(customerInfo.length != 9){
			throw new LogHandlerException("bad log file");
		}
		
		int customerCodeIndex = 4;
		int nameIndex = 2;
		int mobileNumIndex = 3;
		int locXIndex = 5;
		int locYIndex = 6;
		
		int locationX = 0; 
		int locationY = 0;
		
		try{
			locationX = Integer.parseInt(customerInfo[locXIndex]);
			
	    }catch(NumberFormatException e){
	        throw new LogHandlerException("X or Y location not an integer");
	    }
		
		
		try{
		
			locationY = Integer.parseInt(customerInfo[locYIndex]);
	    }catch(NumberFormatException e){
	        throw new LogHandlerException("X or Y location not an integer");
	    }
		
		return CustomerFactory.getCustomer(customerInfo[customerCodeIndex], customerInfo[nameIndex], customerInfo[mobileNumIndex], locationX, locationY);
	}
	
	/**
	 * Creates a Pizza object by parsing the information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Pizza object containing the information from the line in the log file
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException - If there was a problem parsing the line from the log file.
	 */
	public static Pizza createPizza(String line) throws PizzaException, LogHandlerException{
		// TO DO
		String PizzaText[] = line.split(",");
			
		if(PizzaText.length != 9 ){
			throw new LogHandlerException("Bad LogFile");
		}
		
				int oderTimeIndex= 0;
				int deliveryTimeIndex = 1;
				int pizzaTypeIndex = 7;
				int quantityIndex = 8;
				
				int pizzaQuantity;
				LocalTime oderTime;
				LocalTime deliveryTime;
				//parse times
				try{
					oderTime = LocalTime.parse(PizzaText[oderTimeIndex]);					
			    
			       
			    
				
					deliveryTime = LocalTime.parse(PizzaText[deliveryTimeIndex]);			
			    }catch(DateTimeParseException e){
			        throw new LogHandlerException("Times are not in a valid format");
			    }
				
				//parse quantity
				try{
					pizzaQuantity = Integer.parseInt(PizzaText[quantityIndex]);
				}catch(NumberFormatException e){
					 throw new LogHandlerException("Quantity not an int");
				}
				return PizzaFactory.getPizza(PizzaText[pizzaTypeIndex], pizzaQuantity, oderTime, deliveryTime);
			}

}
