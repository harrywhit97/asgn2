package asgn2GUIs;

import java.awt.event.ActionEvent;


import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultCaret;


import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;

import javax.swing.JFrame;

import java.awt.*;
import javax.swing.*;


/**
 * This class is the graphical user interface for the rest of the system. 
 * Currently it is a �dummy� class which extends JFrame and implements Runnable and ActionLister. 
 * It should contain an instance of an asgn2Restaurant.PizzaRestaurant object which you can use to 
 * interact with the rest of the system. You may choose to implement this class as you like, including changing 
 * its class signature � as long as it  maintains its core responsibility of acting as a GUI for the rest of the system. 
 * You can also use this class and asgn2Wizards.PizzaWizard to test your system as a whole
 * 
 * 
 * @author Person A and Person B
 *
 */
public class PizzaGUI extends javax.swing.JFrame implements Runnable, ActionListener {
	

    private javax.swing.JButton btn_load;
    private javax.swing.JButton btn_Display;
    private javax.swing.JButton btn_Calculate;
    private javax.swing.JButton btn_Reset;
   // private javax.swing.JLabel lbl_text;
    private javax.swing.JTextPane txt_Box;
    private javax.swing.JScrollPane scr_Box;
	private PizzaRestaurant p;
	private PizzaRestaurant restaurant;
	private boolean logLoaded;


	/**
	 * Creates a new Pizza GUI with the specified title 
	 * @param title - The title for the supertype JFrame
	 */
	public PizzaGUI(String title) {
		initComponents();
		this.setTitle(title);
		this.logLoaded = false;
		
	}
	
	@Override
	public void run() {
		PizzaGUI frame = new PizzaGUI("test");
        frame.setVisible(true);
        
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource().equals(btn_load)){
			//lbl_text.setText("load");
			
		    JFileChooser chooser = new JFileChooser();
		    FileNameExtensionFilter filter = new FileNameExtensionFilter(
		        "txt log files", "txt");
		    chooser.setFileFilter(filter);
		    int returnVal = chooser.showOpenDialog(getParent());
		    if(returnVal == JFileChooser.APPROVE_OPTION) {
		       
		    	p = new PizzaRestaurant();
		    	try {
					p.processLog(chooser.getSelectedFile().getAbsolutePath());
					this.logLoaded = true;
				} catch (CustomerException | PizzaException | LogHandlerException e1) {
					System.out.println(e1.getMessage());
					
					this.logLoaded = false;
				}
		    	txt_Box.setText("loaded");
		    }	
			
			
			
		}else if(e.getSource().equals(btn_Display) && logLoaded){
			String bigString = "Customer Name \t Mobile Number \t Customer Type \t X Location \t Y Location \t"
					+ "Delivery Distance \t Pizza Type \t Quantity \t Order Price \t Oder Cost \t Order Profit\n";
			for(int i = 0; i < p.getNumCustomerOrders(); i++){
				Customer c = null;
				Pizza pz = null;
				try {
					c = p.getCustomerByIndex(i);
					pz = p.getPizzaByIndex(i);
				} catch (CustomerException | PizzaException e1) {
					System.out.println(e1.getMessage());
					
				}
				if (c.getName().length() <= 8){
				bigString += (c.getName() + "\t\t" + c.getMobileNumber() + "\t\t" + c.getCustomerType()
				+ "\t" + c.getLocationX() + "\t" + c.getLocationY() + "\t" + Math.round(c.getDeliveryDistance()*10000.0)/10000.0 
				+ "\t\t" + pz.getPizzaType() + "\t" + pz.getQuantity() + "\t" + pz.getOrderPrice()
				+ "\t" + pz.getOrderCost() + "\t" + pz.getOrderProfit() + "\n");
				}else{
					bigString += (c.getName() + "         \t" + c.getMobileNumber() + "\t\t" + c.getCustomerType()
					+ "\t" + c.getLocationX() + "\t" + c.getLocationY() + "\t" + Math.round(c.getDeliveryDistance()*10000.0)/10000.0 
					+ "\t\t" + pz.getPizzaType() + "\t" + pz.getQuantity() + "\t" + pz.getOrderPrice()
					+ "\t" + pz.getOrderCost() + "\t" + pz.getOrderProfit() + "\n");
					}//end if statment
				}//end for loop
			
			//lbl_text.setText(bigString);
			txt_Box.setText(bigString);
			
	
		}else if(e.getSource().equals(btn_Calculate) && logLoaded ){
			//lbl_text.setText("Total distance: " + p.getTotalDeliveryDistance() + "\nTotal profit: " + p.getTotalProfit());
			txt_Box.setText("Total distance: " + p.getTotalDeliveryDistance() + "\nTotal profit: " + p.getTotalProfit());
		}else if(e.getSource().equals(btn_Reset)){
			
			//lbl_text.setText("");
			txt_Box.setText("");
		}
		
		//lbl_text.setVisible(true);
		//txt_Box.setVisible(true);
		//scr_Box.setVisible(true);
		
	}
		
	
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        btn_load = new javax.swing.JButton();
        btn_Display = new javax.swing.JButton();
        btn_Calculate = new javax.swing.JButton();
        btn_Reset = new javax.swing.JButton();
     //   lbl_text = new javax.swing.JLabel();
        txt_Box = new javax.swing.JTextPane();
       // scr_Box = new javax.swing.JScrollPane(txt_Box);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btn_load.setText("Load");

        btn_Display.setText("Display");

        btn_Calculate.setText("Calculate");

        btn_Reset.setText("Reset");

        //lbl_text.setBackground(new java.awt.Color(255, 255, 255));
     //   lbl_text.setText("TEXT");
        
        txt_Box.setBackground(new java.awt.Color(255, 255, 255));
        txt_Box.setText("");
//        scr_Box.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
       // txt_Box.setMaximumSize(new Dimension(1200,500));
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_load)
                    .addComponent(btn_Reset)
                    .addComponent(btn_Calculate)
                    .addComponent(btn_Display))
                .addGap(48, 48, 48)
               // .addComponent(lbl_text, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txt_Box, javax.swing.GroupLayout.PREFERRED_SIZE, 1200, javax.swing.GroupLayout.PREFERRED_SIZE)
                //.addComponent(scr_Box, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(83, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                   // .addComponent(lbl_text, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                	.addComponent(txt_Box, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,300)
                	//.addComponent(scr_Box, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, 300)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_load)
                        .addGap(18, 18, 18)
                        .addComponent(btn_Display)
                        .addGap(26, 26, 26)
                        .addComponent(btn_Calculate)
                        .addGap(26, 26, 26)
                        .addComponent(btn_Reset)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        txt_Box.setMaximumSize(new java.awt.Dimension(1200, 300));
        btn_load.addActionListener(this);
        btn_Display.addActionListener(this);
        btn_Calculate.addActionListener(this);
        btn_Reset.addActionListener(this);
        txt_Box.setEditable(false);
        pack();
    }// </editor-fold>    
}
